# TP-Git

## ÉNONCÉ:

- Créer un projet groupeX sur Gitlab.com, y inviter les membres du groupe
- Ecrire le README.md selon la nomenclature en y précisant votre groupe, les membres et l'énoncé.
- Tirer une branche "developpement" de la branche "main"
- Tirer une branche "feature<Nom-de-l-auditeur>" de la branche "develop" - ex: featureLebreton
- Créer un fichier mon-script-<nom-de-l-auditeur>.sh - ex: mon-script-lebreton.sh
- Ajouter
```bash
    #!/bin/bash
    # remplacez le X de groupe X par le numéro de votre groupe
    echo "Hello du groupe X"
    firefox https://www.legaragenumerique.fr/
```	 

- Utiliser les commandes:
```bash
git add *
git commit -m "commentaire"
git push
```

## DOCUMENTATION

- [ ] [Documentation Git sur les branches](https://git-scm.com/docs/git-branch/fr)
- [ ] [Documentation Git sur le commit](https://git-scm.com/docs/git-commit/fr)
- [ ] [Documentation Git sur le push](https://git-scm.com/docs/git-push/fr)

## EXEMPLE

Un exemple du script ainsi que la nomenclature à respecter pour le README.md sont sur la branche featureLebreton.
